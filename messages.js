const express = require('express');
const dataBase = require('./data-base.js');
const fs = require("fs");

const router = express.Router();



router.get('/', (req, res) => {
    let array = [];
    const messages = dataBase.getLastFiles();
    array = messages.slice(-5);
   return res.send(array);
});

router.post('/', (req, res) => {
    const dateTime = new Date().toUTCString();
    const newMessage = {...req.body, dateTime};
    const filePath = `./messages/${dateTime}.txt`;
    dataBase.createFile(filePath, newMessage);
    return res.send(newMessage);
});

module.exports = router;