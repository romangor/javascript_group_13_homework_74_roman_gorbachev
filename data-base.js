const fs = require('fs');

const path = './messages';

let fileNames = [];

module.exports = {
    createFile(filePath, message) {
        fs.writeFileSync(filePath, JSON.stringify(message));
    },

    getLastFiles() {
        fs.readdir(path, (err, files) => {
            files.forEach(fileName => {
                const dataFile = fs.readFileSync(`${path}/${fileName}`);
                fileNames.push(JSON.parse(dataFile));
            });
        });
        return fileNames;
    },

};
