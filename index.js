const express = require('express');
const messages = require('./messages.js');
const dataBase = require('./data-base.js');

const app = express();
const port = 8000;

app.use(express.json());
app.use('/messages', messages);


app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
});